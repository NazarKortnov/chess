from enum import Enum
from typing import Text, List

from pydantic import BaseModel


class ChessPiece:
    def __init__(self, *, color: Text = None, name: Text, icon: Text):
        self.color = color
        self.name = name
        self.icon = icon


class Names(Enum):
    ROOK = "rook"
    KNIGHT = "knight"
    BISHOP = "bishop"
    KING = "king"
    QUEEN = "queen"
    PAWN = "pawn"


class Colors(Enum):
    WHITE = "white"
    BLACK = "black"


class Chessboard(BaseModel):
    square: List


white_rook = ChessPiece(color=Colors.WHITE.value, name=Names.ROOK.value, icon="♖")
white_knight = ChessPiece(color=Colors.WHITE.value, name=Names.KNIGHT.value, icon="♘")
white_bishop = ChessPiece(color=Colors.WHITE.value, name=Names.BISHOP.value, icon="♗")
white_king = ChessPiece(color=Colors.WHITE.value, name=Names.KING.value, icon="♔")
white_queen = ChessPiece(color=Colors.WHITE.value, name=Names.QUEEN.value, icon="♕")
white_pawn = ChessPiece(color=Colors.WHITE.value, name=Names.PAWN.value, icon="♙")
black_rook = ChessPiece(color=Colors.BLACK.value, name=Names.ROOK.value, icon="♜")
black_knight = ChessPiece(color=Colors.BLACK.value, name=Names.KNIGHT.value, icon="♞")
black_bishop = ChessPiece(color=Colors.BLACK.value, name=Names.BISHOP.value, icon="♝")
black_king = ChessPiece(color=Colors.BLACK.value, name=Names.KING.value, icon="♚")
black_queen = ChessPiece(color=Colors.BLACK.value, name=Names.QUEEN.value, icon="♛")
black_pawn = ChessPiece(color=Colors.BLACK.value, name=Names.PAWN.value, icon="♟")
