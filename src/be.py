# Import libraries
from pprint import pprint
from models.objects import *


# Draw the chessboard
def draw(chessboard, letter_coordinates):
    # Define how the output format would draw the chessboard
    form = '[{:>1}]'
    for rows in range(8, 0, -1):
        print()
        for columns in range(1, 9):
            location = letter_coordinates[columns - 1] + str(rows)
            # Check if a chess piece is occupying the square
            if chessboard[location] is None:
                print(form.format(""), end="", flush=True)
            else:
                # Print the chess piece icon on the chessboard
                print(form.format(chessboard[location].icon), end="", flush=True)
    print("\n ")


# Set up the chessboard
def setup(chessboard, letter_coordinates, player_color):
    # Setup chessboard
    for rows in range(8, 0, -1):
        for columns in range(1, 9):

            location = letter_coordinates[columns - 1] + str(rows)
            if player_color == Colors.WHITE.value:
                if rows == 7:
                    chessboard[location] = black_pawn

                elif rows == 2:
                    chessboard[location] = white_pawn

                elif (columns == 1 and rows == 1) or (columns == 8 and rows == 1):
                    chessboard[location] = white_rook

                elif (columns == 1 and rows == 8) or (columns == 8 and rows == 8):
                    chessboard[location] = black_rook

                elif (columns == 2 and rows == 1) or (columns == 7 and rows == 1):
                    chessboard[location] = white_knight

                elif (columns == 2 and rows == 8) or (columns == 7 and rows == 8):
                    chessboard[location] = black_knight

                elif (columns == 3 and rows == 1) or (columns == 6 and rows == 1):
                    chessboard[location] = white_bishop

                elif (columns == 3 and rows == 8) or (columns == 6 and rows == 8):
                    chessboard[location] = black_bishop

                elif columns == 4 and rows == 1:
                    chessboard[location] = white_queen

                elif columns == 4 and rows == 8:
                    chessboard[location] = black_queen

                elif columns == 5 and rows == 1:
                    chessboard[location] = white_king

                elif columns == 5 and rows == 8:
                    chessboard[location] = black_king
                else:
                    chessboard[location] = None

            elif player_color == Colors.BLACK.value:
                if rows == 7:
                    chessboard[location] = white_pawn

                elif rows == 2:
                    chessboard[location] = black_pawn

                elif (columns == 1 and rows == 1) or (columns == 8 and rows == 1):
                    chessboard[location] = black_rook

                elif (columns == 1 and rows == 8) or (columns == 8 and rows == 8):
                    chessboard[location] = white_rook

                elif (columns == 2 and rows == 1) or (columns == 7 and rows == 1):
                    chessboard[location] = black_knight

                elif (columns == 2 and rows == 8) or (columns == 7 and rows == 8):
                    chessboard[location] = white_knight

                elif (columns == 3 and rows == 1) or (columns == 6 and rows == 1):
                    chessboard[location] = black_bishop

                elif (columns == 3 and rows == 8) or (columns == 6 and rows == 8):
                    chessboard[location] = white_bishop

                elif columns == 4 and rows == 1:
                    chessboard[location] = black_queen

                elif columns == 4 and rows == 8:
                    chessboard[location] = white_queen

                elif columns == 5 and rows == 1:
                    chessboard[location] = black_king

                elif columns == 5 and rows == 8:
                    chessboard[location] = white_king
                else:
                    chessboard[location] = None

    pprint(chessboard)


# Move a chess piece from one position to another
def move(chessboard):
    # User pawn selection input
    print("Please select a pawn location in algebraic notation: ")
    pawn_first_location = input()

    # User pawn selection validation
    while pawn_first_location not in chessboard or chessboard[pawn_first_location] is None:
        print("Invalid selection. Please select a pawn location in algebraic notation: ")
        pawn_first_location = input()

    # User square selection input
    print("Please select a square location in algebraic notation to move the piece to: ")
    pawn_second_location = input()

    # User square selection validation.
    while True:
        # Validate if the chess piece is in the chessboard.
        if pawn_second_location not in chessboard:
            print("Invalid selection. Pawn is not in chessboard. Please select a pawn location in algebraic notation: ")
            pawn_second_location = input()

        # Validate if the second location is the same as the first location
        elif pawn_second_location == pawn_first_location:
            print("Invalid selection. You've selected the same chess piece. "
                  "Please select a pawn location in algebraic notation: ")
            pawn_second_location = input()

        # Validate if the second location is occupied by the player's pawn
        elif chessboard[pawn_second_location] is not None:
            if chessboard[pawn_second_location].color == chessboard[pawn_first_location].color:
                print("Invalid selection. Your pawn is located in this square. "
                      "Please select a pawn location in algebraic notation: ")
                pawn_second_location = input()

            # Since the pawn is not the player's color, and must be the enemy pawn, break
            else:
                break

        # Pass validation
        else:
            break
    # If the second location is an enemy piece, announce a capture
    if chessboard[pawn_second_location] is not None:
        pprint("The " + chessboard[pawn_first_location].color + " " + chessboard[pawn_first_location].name +
               " captured the " + chessboard[pawn_second_location].color + " " + chessboard[pawn_second_location].name +
               " at " + pawn_second_location + "!")

    # Copy the pawn in the first coordinate to the second coordinate
    chessboard[pawn_second_location] = chessboard[pawn_first_location]

    # Empty the first coordinate
    chessboard[pawn_first_location] = None

    # Announce the action
    pprint("The " + chessboard[pawn_second_location].color + " " + chessboard[pawn_second_location].name +
           " was moved from " + pawn_first_location + " to " + pawn_second_location + ".")


# Draw the chessboard with a selected chess piece and its legal moves
def threat(chessboard, letter_coordinates, player_color):
    # Define a threat list that holds the pawns and their locations that are being threatened by the selected
    # chess piece
    threat_list = {}

    # User pawn input
    print("Please select a pawn location in algebraic notation: ")
    pawn_location = input()

    # User pawn selection validation
    while pawn_location not in chessboard or chessboard[pawn_location] is None:
        print("Invalid selection. Please select a pawn location in algebraic notation: ")
        pawn_location = input()

    # Define how the output format would draw the chessboard
    form = '[{:>1}]'
    for rows in range(8, 0, -1):
        print()
        for columns in range(1, 9):
            location = letter_coordinates[columns - 1] + str(rows)

            # Check if the chessboard square is not empty
            if chessboard[location] is not None:

                # Threat pawn
                if chessboard[pawn_location].name == white_pawn.name:

                    # Check if the current pawn color in the square is the same as the player selected color and if
                    # the square is a valid pawn location move
                    if (ord(pawn_location[0]) == ord(location[0]) + 1 or ord(pawn_location[0]) == ord(location[0]) - 1) \
                            and pawn_location[1] == str(rows - 1) and player_color == chessboard[pawn_location].color \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # Check if the current pawn color in the square is not the same as the player selected color and if
                    # the square is a valid pawn location move
                    elif (ord(pawn_location[0]) == ord(location[0]) + 1 or ord(pawn_location[0]) == ord(
                            location[0]) - 1) \
                            and pawn_location[1] == str(rows + 1) and player_color != chessboard[pawn_location].color \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)

                # Threat knight
                elif chessboard[pawn_location].name == white_knight.name:
                    if (ord(pawn_location[0]) == ord(location[0]) + 1 and pawn_location[1] == str(rows - 2) or
                        ord(pawn_location[0]) == ord(location[0]) - 1 and pawn_location[1] == str(rows - 2) or
                        ord(pawn_location[0]) == ord(location[0]) + 2 and pawn_location[1] == str(rows - 1) or
                        ord(pawn_location[0]) == ord(location[0]) - 2 and pawn_location[1] == str(rows - 1) or
                        ord(pawn_location[0]) == ord(location[0]) + 1 and pawn_location[1] == str(rows + 2) or
                        ord(pawn_location[0]) == ord(location[0]) - 1 and pawn_location[1] == str(rows + 2) or
                        ord(pawn_location[0]) == ord(location[0]) + 2 and pawn_location[1] == str(rows + 1) or
                        ord(pawn_location[0]) == ord(location[0]) - 2 and pawn_location[1] == str(rows + 1)) \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)

                # Threat king
                elif chessboard[pawn_location].name == white_king.name:
                    if str(rows - 1) <= pawn_location[1] <= str(rows + 1) and \
                            ord(location[0]) - 1 <= ord(pawn_location[0]) <= ord(location[0]) + 1 and chessboard[
                        location] \
                            is not chessboard[pawn_location] \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)

                # Threat rook
                elif chessboard[pawn_location].name == white_rook.name:
                    if (pawn_location[0] == location[0] or pawn_location[1] == location[1]) \
                            and location != pawn_location \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)

                # Threat bishop
                elif chessboard[pawn_location].name == white_bishop.name:
                    if ((rows + columns == ord(pawn_location[0]) - 96 + int(pawn_location[1])) or
                        (ord(pawn_location[0]) - 96 - columns == int(pawn_location[1]) - rows)) \
                            and location != pawn_location \
                            and chessboard[location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)

                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)

                # Threat queen
                elif chessboard[pawn_location].name == white_queen.name:
                    if ((rows + columns == ord(pawn_location[0]) - 96 + int(pawn_location[1])) or (
                            ord(pawn_location[0]) - 96 - columns == int(pawn_location[1]) - rows) or pawn_location[0] ==
                        location[0] or pawn_location[1] == location[1]) and location != pawn_location and chessboard[
                        location].color != chessboard[pawn_location].color:
                        threat_list[location] = chessboard[location].name
                        print(form.format("X"), end="", flush=True)
                    # If the location is not being threatened, draw the chess piece
                    else:
                        print(form.format(chessboard[location].icon), end="", flush=True)
            else:
                print(form.format(""), end="", flush=True)

    print("\n ")
    print(threat_list)


def main():
    # Define the chessboard
    chessboard = {}

    # Define translation for algebraic notation
    letter_coordinates = "abcdefgh"

    # Define chessboard colors
    colors = [item.value for item in Colors]

    # User color selection input
    print(f"Please select a color {colors}")
    player_color = input()

    # Validate pawn color selection
    while player_color not in colors:
        print(f"Wrong color. Please select a color {Colors.__members__.values()}")
        player_color = input()

    # Set opposite color
    pawn_opposite_colors = list(colors)
    pawn_opposite_colors.remove(player_color)

    # Define enemy color
    enemy_color = str(pawn_opposite_colors).strip("'[]")

    # Announce player color and enemy color
    print("You're playing as " + player_color + "! Your enemy is " + enemy_color + ".")

    # Set up the chessboard
    setup(chessboard, letter_coordinates, player_color)

    # Announce that the chessboard has been set up.
    print("The chessboard has been setup.")

    # Basic menu selection
    while 1 != 999:
        print("\r 1. Draw the chessboard \n\r 2. Move a chess piece \n\r 3. Check threat \n\r 4. Quit")
        menu_selection = input()
        if menu_selection == '1':
            draw(chessboard, letter_coordinates)
        elif menu_selection == '2':
            move(chessboard)
        elif menu_selection == '3':
            threat(chessboard, letter_coordinates, player_color)
        elif menu_selection == '4':
            exit(1)
        else:
            print("invalid selection.")


if __name__ == '__main__':
    main()
